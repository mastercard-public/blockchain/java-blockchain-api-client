# Mastercard Blockchain Client

A simple console application which demonstrates how to use the Mastercard Blockchain. 

For details, see the SDK reference documentation at https://developer.mastercard.com/documentation/blockchain/1




## Get Started

This section explains how to use this application to access the Blockchain API hosted on https://developer.mastercard.com 

### Prerequisites

- Maven 3.6.0+
- JDK 1.8.0+
- Protocol Buffers (download binaries from https://github.com/protocolbuffers/protobuf/releases) 
- A developer account on https://developer.mastercard.com and a developer project for Mastercard Blockchain


### Generate a Sandbox Key for your Project

- Login to https://developer.mastercard.com
- In the menu on the left, navigate to 'My Projects'
- Click 'Generate New Project'
- Choose the 'Blockchain' API
- Give your project a name
- Download the generated PKCS#12 keystore
- Take a note of the keystore password and key alias
- On the next screen, take a note of the consumer key linked to your certificate

### Configure your Project

- Copy the downloaded PKCS#12 keystore to src/main/resources
- Configure the application.yml file:
  - set the consumer key
  - set the key file name
  
### Compile and Run the Blockchain Client

    mvn clean package
    java -jar target/blockchain-api-client-0.0.1-SNAPSHOT.jar





 

