# NodeApi

All URIs are relative to *https://api.mastercard.com/labs/proxy/chain/api/network*

Method | HTTP request | Description
------------- | ------------- | -------------
[**nodesAddressGet**](NodeApi.md#nodesAddressGet) | **GET** /nodes/{address} | Gets information about a specific node on the network
[**nodesGet**](NodeApi.md#nodesGet) | **GET** /nodes | Gets information about your local blockchain node


<a name="nodesAddressGet"></a>
# **nodesAddressGet**
> NodeInfo nodesAddressGet(address)

Gets information about a specific node on the network

Information about a specific node may be retrieved by its address. This is useful when navigating the network. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.NodeApi;


NodeApi apiInstance = new NodeApi();
String address = CNkNVuVnQ4WigadrQKcNuTa1JkAJFWF9S8; // String | 
try {
    NodeInfo result = apiInstance.nodesAddressGet(address);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NodeApi#nodesAddressGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **String**|  |

### Return type

[**NodeInfo**](NodeInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="nodesGet"></a>
# **nodesGet**
> NodeInfo nodesGet()

Gets information about your local blockchain node

This call gets information about your local blockchain node, the applications that it understands, and its connections to other peers in the network. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.NodeApi;


NodeApi apiInstance = new NodeApi();
try {
    NodeInfo result = apiInstance.nodesGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NodeApi#nodesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NodeInfo**](NodeInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

