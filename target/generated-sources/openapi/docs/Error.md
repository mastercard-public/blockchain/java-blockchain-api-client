
# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**details** | **String** |  |  [optional]
**reasonCode** | **String** |  |  [optional]
**recoverable** | **Boolean** |  |  [optional]
**source** | **String** |  |  [optional]



