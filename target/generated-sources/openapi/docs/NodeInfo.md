
# NodeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** | The address of this node |  [optional]
**authority** | **String** | The signing identity from the audit nodes, either as a blockchain address, or as the raw public key |  [optional]
**publicKey** | **String** | Public key of node. Can be used to validate signatures from this node. |  [optional]
**type** | **String** | The type of Blockchain node audit/customer or unknown |  [optional]
**chainHeight** | **Integer** | Last verified slot on the chain as seen by this node |  [optional]
**entryInjectionOffset** | **Integer** | Entry insertion offset - the number of blocks in the future when an entry is expected to be processed |  [optional]
**unconfirmed** | **Integer** | The number of blockchain entries awaiting confirmation |  [optional]
**peers** | [**List&lt;Connection&gt;**](Connection.md) | Connected nodes |  [optional]



