
# MessagesApiResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | The http status of the call |  [optional]



