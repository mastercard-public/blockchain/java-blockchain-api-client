# BlockApi

All URIs are relative to *https://api.mastercard.com/labs/proxy/chain/api/network*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocksGet**](BlockApi.md#blocksGet) | **GET** /blocks | Get information about the last confirmed block or a range of blocks
[**blocksIdGet**](BlockApi.md#blocksIdGet) | **GET** /blocks/{id} | Gets a specified block by id


<a name="blocksGet"></a>
# **blocksGet**
> List&lt;Block&gt; blocksGet(offset, count)

Get information about the last confirmed block or a range of blocks

This call returns the last confirmed block, or a range of blocks.  To get a range of blocks, use the &#x60;offset&#x60; and &#x60;count&#x60; parameters. Specifying only the &#x60;offset&#x60; parameter will get a range of blocks up to the current slot, or up to the maximum count.  Note that the maximum value for &#39;count&#39; is 600.  For each returned item, the data will contain header information from the block binary, and references to the block entries via the merkle roots. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.BlockApi;


BlockApi apiInstance = new BlockApi();
Long offset = 0; // Long | Specify the starting slot to return
Integer count = 50; // Integer | The desired number of slots
try {
    List<Block> result = apiInstance.blocksGet(offset, count);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#blocksGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **Long**| Specify the starting slot to return | [optional]
 **count** | **Integer**| The desired number of slots | [optional]

### Return type

[**List&lt;Block&gt;**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="blocksIdGet"></a>
# **blocksIdGet**
> Block blocksIdGet(id)

Gets a specified block by id

A specific block may be retrieved by its hash key or by its slot number. This is useful when navigating the chain. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.BlockApi;


BlockApi apiInstance = new BlockApi();
String id = 1503574734; // String | Id can be the slot number or the hash of the block to retrieve
try {
    Block result = apiInstance.blocksIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BlockApi#blocksIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Id can be the slot number or the hash of the block to retrieve |

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

