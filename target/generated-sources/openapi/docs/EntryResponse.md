
# EntryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** | Hash of the value written onto the chain. Can be used to lookup the entry. |  [optional]
**status** | **String** | Status of the entry on the chain pending\\confirmed and rejected. |  [optional]



