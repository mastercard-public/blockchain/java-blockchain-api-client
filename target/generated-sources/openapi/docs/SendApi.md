# SendApi

All URIs are relative to *https://api.mastercard.com/labs/proxy/chain/api/network*

Method | HTTP request | Description
------------- | ------------- | -------------
[**messagesPost**](SendApi.md#messagesPost) | **POST** /messages | Sends a message directly to one or more peers


<a name="messagesPost"></a>
# **messagesPost**
> MessagesApiResponse messagesPost(sendRequest)

Sends a message directly to one or more peers

Send a message directly to one or more peers, via the application-specific back-channel. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.SendApi;


SendApi apiInstance = new SendApi();
SendRequest sendRequest = new SendRequest(); // SendRequest | 
try {
    MessagesApiResponse result = apiInstance.messagesPost(sendRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SendApi#messagesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sendRequest** | [**SendRequest**](SendRequest.md)|  |

### Return type

[**MessagesApiResponse**](MessagesApiResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

