
# DataPartition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | **String** | Application identifier |  [optional]
**entryCount** | **Integer** | The number of entries referred to by the merkle root hash |  [optional]
**merkleRoot** | **String** | Its the root hash of all entries for this application in this block |  [optional]
**entries** | **List&lt;String&gt;** | List of hashes for all entries in this application partition |  [optional]



