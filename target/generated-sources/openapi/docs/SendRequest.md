
# SendRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **String** | This is the application id. You can get a list of applications your Blockchain node is part of by using the Status/query API | 
**encoding** | **String** | Encoding scheme used to encode the value being sent. Must be base58, base64 or hex | 
**value** | **String** | Value to be sent; should be encoded according to specified encoding scheme above | 
**peers** | [**List&lt;PeerLocator&gt;**](PeerLocator.md) | The blockchain addresses of the nodes to which the message should be sent | 



