
# EntryRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **String** | This is the application id. You can get a list of applications your Blockchain node is part of by using the Status/query API | 
**encoding** | [**EncodingEnum**](#EncodingEnum) | Encoding scheme used to encode the value being sent. Must be base58, base64 or hex | 
**value** | **String** | Value to be written onto the chain. Value should be encoded according to specified encoding scheme above | 


<a name="EncodingEnum"></a>
## Enum: EncodingEnum
Name | Value
---- | -----
BASE58 | &quot;base58&quot;
BASE64 | &quot;base64&quot;
HEX | &quot;hex&quot;



