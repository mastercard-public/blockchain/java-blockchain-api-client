
# CurrentBlock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slot** | **Long** | The slot number of the last confirmed block |  [optional]
**ref** | **String** | Hash of the last confirmed block |  [optional]



