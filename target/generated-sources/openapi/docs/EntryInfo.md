
# EntryInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** | Hash of the value written onto the chain. Can be used to lookup the entry. |  [optional]
**slot** | **Long** | Slot the entry is confirmed in; 0 if none. |  [optional]
**status** | **String** | Status of the entry on the chain pending\\confirmed or rejected. |  [optional]
**value** | **String** | Hex representation of the value written onto the chain. |  [optional]



