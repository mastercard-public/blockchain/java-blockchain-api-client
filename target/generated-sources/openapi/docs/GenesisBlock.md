
# GenesisBlock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slot** | **Long** | The slot number of the first block in the chain on this network |  [optional]
**ref** | **String** | Hash of the first block in the chain on this network |  [optional]



