
# Block

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** | The hash of this block |  [optional]
**slot** | **Long** | The slot number of this block |  [optional]
**version** | **String** | The version of the block format. Currently, 1.0.0.0 |  [optional]
**previousBlock** | **String** | The SHA256D hash of the previous block, encoded as hex |  [optional]
**partitions** | [**List&lt;DataPartition&gt;**](DataPartition.md) |  |  [optional]
**nonce** | **String** | Random number used in the generation of this block |  [optional]
**authority** | **String** | The signing identity from the audit nodes as a blockchain address |  [optional]
**signature** | **String** | The signature confirmation from the audit nodes |  [optional]



