
# StatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network** | **Integer** | For the main chain, this id should always be the integer 1513115205.  |  [optional]
**version** | **String** | Note that this is returned as a semantically versioned string. Versions of messages should be able to be transmitted on the binary protocol as 32-bit integers. e.g. The version \&quot;1.0.0.0\&quot; represents the actual big-endian byte array [1,0,0,0] which is equivalent to a uint32 value of 16777216.  |  [optional]
**witnesses** | **List&lt;String&gt;** | The blockchain addresses of audit nodes which have signed block confirmations. |  [optional]
**applications** | **List&lt;String&gt;** | The application identifiers available at this node |  [optional]
**genesis** | [**GenesisBlock**](GenesisBlock.md) |  |  [optional]
**current** | [**CurrentBlock**](CurrentBlock.md) |  |  [optional]
**alerts** | **List&lt;String&gt;** | Informational alerts such as upcoming version or network configuration changes |  [optional]



