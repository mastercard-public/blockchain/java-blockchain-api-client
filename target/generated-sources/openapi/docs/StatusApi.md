# StatusApi

All URIs are relative to *https://api.mastercard.com/labs/proxy/chain/api/network*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generalNetworkStatusGet**](StatusApi.md#generalNetworkStatusGet) | **GET** /general-network-status | Gets general network status information


<a name="generalNetworkStatusGet"></a>
# **generalNetworkStatusGet**
> StatusResponse generalNetworkStatusGet()

Gets general network status information

Gets general metadata about the current state of the blockchain network. Useful for building dashboards. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.StatusApi;


StatusApi apiInstance = new StatusApi();
try {
    StatusResponse result = apiInstance.generalNetworkStatusGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusApi#generalNetworkStatusGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

