# TransactionEntryApi

All URIs are relative to *https://api.mastercard.com/labs/proxy/chain/api/network*

Method | HTTP request | Description
------------- | ------------- | -------------
[**entriesHashGet**](TransactionEntryApi.md#entriesHashGet) | **GET** /entries/{hash} | Information about the specified transaction entry
[**entriesPost**](TransactionEntryApi.md#entriesPost) | **POST** /entries | Adds a transaction entry to the blockchain


<a name="entriesHashGet"></a>
# **entriesHashGet**
> EntryInfo entriesHashGet(hash)

Information about the specified transaction entry

Returns full detail of the value of the blockchain entry referenced by the specified hashkey, if it has been previously recorded by this node. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.TransactionEntryApi;


TransactionEntryApi apiInstance = new TransactionEntryApi();
String hash = 1e6fc898c0f0853ca504a29951665811315145415fa5bdfa90253efe1e2977b1; // String | The hash of the entry to retrieve
try {
    EntryInfo result = apiInstance.entriesHashGet(hash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionEntryApi#entriesHashGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hash** | **String**| The hash of the entry to retrieve |

### Return type

[**EntryInfo**](EntryInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="entriesPost"></a>
# **entriesPost**
> EntryResponse entriesPost(entryRequest)

Adds a transaction entry to the blockchain

Add a transaction entry for your application to the blockchain. Note that this entry must be a valid message according to the application configuration that you set up. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.TransactionEntryApi;


TransactionEntryApi apiInstance = new TransactionEntryApi();
EntryRequest entryRequest = new EntryRequest(); // EntryRequest | 
try {
    EntryResponse result = apiInstance.entriesPost(entryRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransactionEntryApi#entriesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entryRequest** | [**EntryRequest**](EntryRequest.md)|  |

### Return type

[**EntryResponse**](EntryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

