package com.mastercard.blockchain.apiclient.config;

import com.mastercard.developer.interceptors.OkHttp2OAuth1Interceptor;
import com.mastercard.developer.utils.AuthenticationUtils;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.openapitools.client.ApiClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.List;

@Configuration
public class ApiClientConfig {


    @Resource
    ApiProperties apiProperties;

    @Bean
    public ApiClient apiClient() throws IOException, NoSuchProviderException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {

        String consumerKey = apiProperties.getConsumerKey();
        String keyAlias = apiProperties.getKeyAlias();
        String keyPassword = apiProperties.getKeyPassword();
        String keyFile = apiProperties.getKeyFile();

        String keyFilePath = "./src/main/resources/" + keyFile;

        PrivateKey signingKey = AuthenticationUtils.loadSigningKey(keyFilePath, keyAlias, keyPassword);

        ApiClient client = new ApiClient();
        client.setBasePath(apiProperties.getBasePath());
        client.setDebugging(apiProperties.isDebug());

        List<Interceptor> interceptors = client.getHttpClient().networkInterceptors();
        interceptors.add(new ForceJsonResponseInterceptor());
        interceptors.add(new OkHttp2OAuth1Interceptor(consumerKey, signingKey));

        return client;
    }


    private class ForceJsonResponseInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            String withJsonFormatUrl = withJsonFormat(originalRequest.uri().toString());
            Request newRequest = originalRequest.newBuilder().url(withJsonFormatUrl).build();
            return chain.proceed(newRequest);
        }

        private String withJsonFormat(String uri) {
            StringBuilder newUri = new StringBuilder(uri);
            newUri.append(uri.contains("?") ? "&" : "?");
            newUri.append("Format=JSON");
            return newUri.toString();
        }
    }
}
