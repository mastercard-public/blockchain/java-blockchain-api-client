package com.mastercard.blockchain.apiclient.ui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static String captureInput(String question) {
        return captureInput(question, null);
    }

    public static String captureInput(String question, String defaultAnswer) {

        if (defaultAnswer == null) {
            defaultAnswer = "";
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        if (defaultAnswer.isEmpty()) {
            System.out.print("    " + question + ": ");
        } else {
            System.out.print("    " + question + " [" + defaultAnswer + "]: ");
        }
        String s;
        try {
            s = br.readLine();
            if (s == null || "".equals(s)) {
                s = defaultAnswer;
            }
        } catch (IOException e) {
            s = defaultAnswer;
        }
        return s;
    }


    public static void printHeading(String heading) {
        System.out.println("\n    ============ " + heading + " ============");
    }

    public static void printMenuItem(String item) {
        System.out.println("    " + item);
    }

    public static void print(String s) {
        System.out.println(s);
    }

    public static void printJson(Object o) {
        print(gson.toJson(o));
    }
}
