package com.mastercard.blockchain.apiclient.ui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mastercard.blockchain.apiclient.Ticker;
import com.mastercard.blockchain.apiclient.config.ConfigProperties;
import com.mastercard.blockchain.message.PB02;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.BlockApi;
import org.openapitools.client.api.NodeApi;
import org.openapitools.client.api.StatusApi;
import org.openapitools.client.api.TransactionEntryApi;
import org.openapitools.client.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Base64;
import java.util.List;
import java.util.Random;

import static com.mastercard.blockchain.apiclient.ui.Console.*;


@Service
@Order(10)
public class Menu implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Menu.class);

    public static final String QUIT = "0";

    @Resource
    ApiClient client;

    @Resource
    ConfigProperties configProperties;

    @Override
    public void run(String... args) throws Exception {
        menu();
    }

    private void menu() {

        String option = "";

        while (!option.equals(QUIT)) {

            printHeading("MENU");
            printMenuItem("1   - Get Status");
            printMenuItem("2   - Get Node Info");
            printMenuItem("21  - Get Node Info - by node address");
            printMenuItem("3   - Get Block - by hash or slot id");
            printMenuItem("31  - Get Block - last slot");
            printMenuItem("32  - Get Block Range");
            printMenuItem("4   - TransactionEntry create");
            printMenuItem("41  - TransactionEntry create - Protobuf message (PB02)");
            printMenuItem("5   - TransactionEntry read - prints raw b64 encoded value");
            printMenuItem("51  - TransactionEntry read - prints raw and decoded value");
            printMenuItem("9   - Ticker - print new blocks as they are confirmed");

            printMenuItem(QUIT + ". Quit");
            printMenuItem("");
            option = captureInput("Select Option");

            try {
                switch (option) {

                    case "1":
                        statusQuery();
                        break;
                    case "2":
                        nodeQuery();
                        break;
                    case "21":
                        nodeQueryWithParameter();
                        break;
                    case "3":
                        blockRead();
                        break;
                    case "31":
                        blockList();
                        break;
                    case "32":
                        blockListWithParameters();
                        break;
                    case "4":
                        postEntry();
                        break;
                    case "41":
                        postEntryPb02();
                        break;
                    case "5":
                        getEntry();
                        break;
                    case "51":
                        getAndDecodeEntry();
                        break;
                    case "9":
                        new Ticker(client).start();
                        break;
                    case QUIT:
                        goodbye();
                        break;
                    default:
                        System.err.println("Unrecognised Option");
                }

            } catch (ApiException e) {
                log.info("Response code: {}, message: {}", e.getCode(), e.getMessage());
            }
        }
    }

    private void statusQuery() throws ApiException {
        printHeading("Get Status");

        StatusApi statusApi = new StatusApi(client);
        StatusResponse statusResponse = statusApi.generalNetworkStatusGet();
        printJson(statusResponse);
    }

    private void nodeQuery() throws ApiException {
        printHeading("Get Node Info");
        NodeApi nodeApi = new NodeApi(client);
        NodeInfo nodeInfo = nodeApi.nodesGet();
        printJson(nodeInfo);
    }
    private void nodeQueryWithParameter() throws ApiException {
        printHeading("Get Node Info - by node address");
        String address = captureInput("Node address");
        NodeApi nodeApi = new NodeApi(client);
        NodeInfo nodeInfo = nodeApi.nodesAddressGet(address);
        printJson(nodeInfo);
    }

    private void blockRead() throws ApiException {
        printHeading("Get Block - by hash or slot id");
        String id = captureInput("Block id (slot or hash)");
        BlockApi blockApi = new BlockApi(client);
        Block block = blockApi.blocksIdGet(id);
        printJson(block);
    }

    private void blockList() throws ApiException {

        printHeading("Get Block - last slot");

        // blockGet without optional parameters returns the last block
        List<Block> blocks = new BlockApi(client).blocksGet(null, null);

        if (blocks.size() > 0) {
            Block block = blocks.get(0);
            printJson(block);
        }
    }

    private void blockListWithParameters() throws ApiException {

        printHeading("Get Block Range");

        Long offset = Long.parseLong(captureInput("Offset"));
        Integer count = Integer.parseInt(captureInput("Count"));

        List<Block> blocks = new BlockApi(client).blocksGet(offset, count);

        if (blocks.size() > 0) {
            blocks.forEach((block) -> printJson(block));
        }
    }

    private void postEntry() throws ApiException {
        printHeading("Post Entry");

        String message = captureInput("Entry Text", "Mastercard Blockchain rocks!");
        String partition = captureInput("Partition", configProperties.getPartition());

        String value = Base64.getEncoder().encodeToString(message.getBytes());

        EntryRequest entryRequest = new EntryRequest()
                .value(value)
                .encoding(EntryRequest.EncodingEnum.BASE64)
                .app(partition);

        EntryResponse entryResponse = new TransactionEntryApi(client).entriesPost(entryRequest);
        printJson(entryResponse);

    }

    private void postEntryPb02() throws ApiException {
        printHeading("Post Entry");

        String message = captureInput("Entry Text", "Mastercard Blockchain rocks!");

        PB02.Message protobufMsg = PB02.Message.newBuilder()
                .setValue((new Random()).nextLong())
                .setOtherValue(message)
                .setTimestamp(System.currentTimeMillis())
                .build();

        String value = Base64.getEncoder().encodeToString(protobufMsg.toByteArray());

        EntryRequest entryRequest = new EntryRequest()
                .value(value)
                .encoding(EntryRequest.EncodingEnum.BASE64)
                .app("PB02");

        EntryResponse entryResponse = null;
        try {
            entryResponse = new TransactionEntryApi(client).entriesPost(entryRequest);
        } catch (ApiException e) {
            log.error("Error: {}, message: {}", e.getCode(), e.getMessage());
        }
        printJson(entryResponse);

    }

    private EntryInfo getEntry() throws ApiException {
        printHeading("Get Entry");

        String hash = captureInput("Entry hash");

        EntryInfo entryInfo = new TransactionEntryApi(client).entriesHashGet(hash);
        printJson(entryInfo);
        return entryInfo;
    }

    private void getAndDecodeEntry() throws ApiException {
        printHeading("Get Entry");

        String hash = captureInput("Entry hash");

        EntryInfo entryInfo = new TransactionEntryApi(client).entriesHashGet(hash);

        String rawValue = entryInfo.getValue();
        String decoded = null;
        try {
            decoded = new String(Hex.decodeHex(rawValue));
        } catch (DecoderException e) {
            log.error("Error while decoding raw value from the API response");
        }

        // manipulate the JSON response
        Gson gson = new GsonBuilder().create();
        JsonObject json = gson.toJsonTree(entryInfo).getAsJsonObject();
        json.addProperty("decoded_value", decoded);

        printJson(gson.fromJson(json, JsonObject.class));
    }

    private void goodbye() {
        print("Mastercard Blockchain rocks!");
    }

}
