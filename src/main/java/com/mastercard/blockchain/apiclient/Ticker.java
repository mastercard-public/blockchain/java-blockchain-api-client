package com.mastercard.blockchain.apiclient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.BlockApi;
import org.openapitools.client.model.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Ticker {

    private static final Logger log = LoggerFactory.getLogger(Ticker.class);

    private static final AtomicLong previous = new AtomicLong(-1);

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static ApiClient client;

    public Ticker(ApiClient client) {
        Ticker.client = client;
    }

    public static void getLatestBlock()  {

        try {

            // blockGet without optional parameters returns the last block
            List<Block> blocks = new BlockApi(client).blocksGet(null, null);

            if (blocks.size() > 0) {
                Block block = blocks.get(0);
                long slot = block.getSlot();
                if (slot > previous.get()) {
                    previous.set(slot);
                    log.info("new block: {}\n{}", slot, gson.toJson(block));
                }
            }

        } catch (ApiException e) {
            log.error("Exception while getting latest block", e);
        }
    }

    public void start() {

        log.info("Ticker starting. Press <Enter> to return to menu");

        // periodically check blockchain progress until input detected
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        executorService.scheduleAtFixedRate(() -> getLatestBlock(), 0, 100, TimeUnit.MILLISECONDS);

        try {
            System.in.read();
            log.info("Ticker stopping.");
        } catch (IOException e) {
            // ignore
        } finally {
            executorService.shutdown();
        }
    }
}