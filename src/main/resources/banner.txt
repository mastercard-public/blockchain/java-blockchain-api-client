                     _                             _       _     _            _        _           _
                    | |                           | |     | |   | |          | |      | |         (_)
 _ __ ___   __ _ ___| |_ ___ _ __ ___ __ _ _ __ __| |     | |__ | | ___   ___| | _____| |__   __ _ _ _ __
| '_ ` _ \ / _` / __| __/ _ \ '__/ __/ _` | '__/ _` |     | '_ \| |/ _ \ / __| |/ / __| '_ \ / _` | | '_ \
| | | | | | (_| \__ \ ||  __/ | | (_| (_| | | | (_| |     | |_) | | (_) | (__|   < (__| | | | (_| | | | | |
|_| |_| |_|\__,_|___/\__\___|_|  \___\__,_|_|  \__,_|     |_.__/|_|\___/ \___|_|\_\___|_| |_|\__,_|_|_| |_|



A bare bones console application to demonstrate how to write and retrieve entries from the blockchain.

For details, see the SDK reference documentation at https://developers.mastercard.com/documentation/blockchain/1

